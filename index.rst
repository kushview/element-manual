.. Element documentation master file, created  on Wed Jul 14 13:56:01 2021.

.. include:: /shortcuts.rstext

.. _index:

Element |version| User Manual
=============================

`This manual is under construction: This manual is a work in progress. Topics might be misplaced
in the navigation. Some sections are more complete than others.`

Element is `free <https://www.gnu.org/philosophy/free-sw.html>`_ plugin software 
for Linux, macOS and Windows.  Defacto modular audio software for routing, 
control, and scrtipting. Control Element with with :term:`MIDI` or your 
computer's keyboard and mouse.

.. toctree::
   :maxdepth: 2
   :caption: Manual

   chapters/introduction
   chapters/ui
   chapters/preferences
   chapters/scripting

.. toctree::
   :maxdepth: 3
   :caption: Developers

   developers/building
   developers/code-style
   developers/lua-style
   Miscellaneous <developers/misc>

.. toctree::
   :maxdepth: 1
   :caption: Appendix

   appendix/osc-commands
   appendix/plugin
   glossary
