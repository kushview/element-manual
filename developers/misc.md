# The Change Log

Dates used in the change log can be generated from the command line:
```bash
date +"%a, %d %b %Y %k:%M:%S +0000"
```