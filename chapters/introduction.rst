Introduction
============

Modular audio software like Element provides a versatile and advanced 
platform for audio plugin hosting, suited for both live performance and studio 
work. Element stands out as a programmable system enabling users to create 
powerful effects racks, and instruments by connecting  nodes to each other. 
This integration can extend to hardware through standard protocols like :term:`MIDI`. 
ensuring broad compatibility and usability in various settings.  The core 
concept is designing your own functionality.

Element runs on Linux, macOS, and Windows supporting a wide range of plugin 
formats including :term:`AU`, :term:`VST`, :term:`VST3`, and :term:`LV2` 
among others. Its flexibility is further demonstrated by its ability to run 
standalone or as a plugin in a digital audio workstation (:term:`DAW`). This 
makes it a valuable tool for users who want to chain plugins together into 
effects racks or instruments in a live or studio setting.  Sessions and graphs
created with Element can be shared across plugin and standalone versions.

Moreover, Element's features are extensive and cater to a variety of needs. They 
include routing audio and MIDI from anywhere to anywhere, external 
synchronization with MIDI Clock, MIDI controller mapping, sub-graphing (nesting 
graphs within each other), custom keyboard shortcuts, placeholder nodes, a 
built-in virtual keyboard, and multiple undo/redo options.

Element has also been open-sourced under the GPL license, indicating a 
community-focused approach to its development and improvement. Users are 
encouraged to contribute to the project, whether by reporting issues, submitting 
pull requests, or joining the development discussion on platforms like Discord 
and GitLab. This community involvement helps in continuously refining and 
enhancing the software to better meet the needs of its diverse user base.

For more detailed information and to explore Element's capabilities, you can 
visit the the website `kushview.net <https://kushview.net/element/>`_ and the 
`GitLab page <https://gitlab.com/kushview/element>`_.
