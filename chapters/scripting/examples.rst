Example Scripts
===============

.. Hello World
.. -----------
.. .. literalinclude:: ../../examples/helloworld.lua
..     :caption: helloworld.lua
..     :name: helloworld-lua
..     :language: lua

Amplifier
---------
.. literalinclude:: ../../examples/amp.lua
    :caption: amp.lua
    :name: amp-lua
    :language: lua

Amplifier UI
------------
.. literalinclude:: ../../examples/ampui.lua
    :caption: ampui.lua
    :name: ampui-lua
    :language: lua

MIDI Filter
-----------
.. literalinclude:: ../../examples/channelize.lua
    :caption: channelize.lua
    :name: midi-filter
    :language: lua
