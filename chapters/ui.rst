UI
===

.. toctree::
    :maxdepth: 1

    ui/plugin-manager
    ui/virtual-keyboard
    ui/automatic-connections
    ui/graph-options
    ui/keyboard-commands
    ui/keyboard-splits
    ui/midi-clock
    ui/vst-presets
