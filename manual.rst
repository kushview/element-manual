.. Element manual index file.

.. include:: /shortcuts.rstext

ELEMENT USERS MANUAL
--------------------

.. toctree::
   :maxdepth: 2

   chapters/introduction
   chapters/ui
   chapters/preferences
   chapters/scripting
   glossary
